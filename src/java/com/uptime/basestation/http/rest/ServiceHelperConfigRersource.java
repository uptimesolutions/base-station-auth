/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.basestation.http.rest;

import com.uptime.basestation.http.client.MonitoringClient;
import com.uptime.basestation.http.utils.json.sax.ServiceHelperJsonParser;
import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.client.utils.vo.HttpResponseVO;
import java.util.List;
import java.util.UUID;
import javax.inject.Singleton;
import javax.ws.rs.core.Context;
import javax.ws.rs.Produces;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * REST Web Service
 *
 * @author twilcox
 */
@Path("/servicehelper")
@Singleton
public class ServiceHelperConfigRersource {
    private final static Logger LOGGER = LoggerFactory.getLogger(ServiceHelperConfigRersource.class);
    
    @GET
    @Path("/hello")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getJson() {
        LOGGER.info("HELLO WORLD");
        return Response.ok("{\"outcome\":\"Hello World!\"}").header("Access-Control-Allow-Origin", "*").build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getJson(@Context HttpHeaders headers, @QueryParam("customer") String customer, @QueryParam("siteId") UUID siteId, @QueryParam("baseStationPort") int port) {
        LOGGER.info("GET");
        HttpResponseVO httpResponseVO;
        String mac = "";
        String token = null;
        
        try {
            if (headers != null){
                // check query parameters
                if(customer != null && siteId != null && port != 0){
                    // get the required HTTP headers to prove this is an authorized device
                    List<String> header = headers.getRequestHeader("HWID");
                    if(header != null && !header.isEmpty())
                        mac = header.get(0);
                    else
                        mac = null;
                    header = headers.getRequestHeader("HWIDTOKEN");
                    if(header != null && !header.isEmpty())
                        token = header.get(0);
                    else
                        token = null;
                    // check the required HTTP headers
                    if(mac != null && !mac.isEmpty() && token != null && !token.isEmpty()){
                        // TODO: validate token
                        if ((httpResponseVO = MonitoringClient.getInstance().getServiceHelperConfig(ServiceRegistrarClient.getInstance().getServiceHostURL("Monitoring"), customer, siteId, port)) != null && 
                                httpResponseVO.getEntity() != null && httpResponseVO.getStatusCode() == 200) {
                                // parse JSON and convert to service helper response
                                String response = ServiceHelperJsonParser.getInstance().formatServiceHelperResponse(httpResponseVO.getEntity());
                                return Response.status(200).entity(response).header("Access-Control-Allow-Origin", "*").build();
                        }
                        else{
                            return Response.status(503).header("Access-Control-Allow-Origin", "*").build();
                        }
                    }
                    else{
                        LOGGER.warn("Required Headers Missing - HWID:"+mac+" HWIDTOKEN:"+token);
                        return Response.status(400).header("Access-Control-Allow-Origin", "*").build();
                    }
                }
                else{
                    LOGGER.warn("Query Parameter Missing - Customer:"+customer+" SiteID:"+siteId+" BaseStationPort:"+port);
                    return Response.status(400).header("Access-Control-Allow-Origin", "*").build();
                }
            }
            else{
                LOGGER.warn("No HTTP Headers");
                return Response.status(400).header("Access-Control-Allow-Origin", "*").build();
            }
        } catch (Exception e) {
            LOGGER.error(mac+" "+e.getMessage(), e);
            return Response.status(500).header("Access-Control-Allow-Origin", "*").build();
        }
    }

    @POST
    public Response postJson() {
        return Response.status(400).header("Access-Control-Allow-Origin", "*").build();
    }
    
    @PUT
    public Response putJson() {
        return Response.status(400).header("Access-Control-Allow-Origin", "*").build();
    }
 
    @DELETE
    public Response deleteJson() {
        return Response.status(400).header("Access-Control-Allow-Origin", "*").build();
    }
}
