/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.basestation.http.rest;

import com.uptime.basestation.http.client.EmailClient;
import com.uptime.basestation.http.utils.json.JsonUtil;
import com.uptime.basestation.utils.vo.BaseStationExceptionVO;
import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.client.utils.vo.HttpResponseVO;
import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.Context;
import javax.ws.rs.Produces;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * REST Web Service
 *
 * @author twilcox
 */
@Path("/exception")
@Singleton
public class BaseStationExceptionRersource {
    private final static Logger LOGGER = LoggerFactory.getLogger(BaseStationExceptionRersource.class);
    
    @GET
    @Path("/hello")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getJson() {
        //LOGGER.info("HELLO WORLD");
        return Response.ok("{\"outcome\":\"Hello World!\"}").header("Access-Control-Allow-Origin", "*").build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response postJson(@Context HttpHeaders headers, String body) {
        String mac;
        String token;
        try{
            if(headers != null && body != null){
                mac = headers.getRequestHeader("HWID").get(0);
                if(mac == null || mac.isEmpty()){
                    LOGGER.warn("Required Header Missing - HWID:"+mac);
                    return Response.status(400).header("Access-Control-Allow-Origin", "*").build();
                }
                token = headers.getRequestHeader("HWIDTOKEN").get(0);
                if(token == null || token.isEmpty()){
                    LOGGER.warn("Required Headers Missing - HWIDTOKEN:"+token);
                    return Response.status(400).header("Access-Control-Allow-Origin", "*").build();
                }
                
                // parse exception json
                BaseStationExceptionVO vo = JsonUtil.parse(body);

                // send email
                HttpResponseVO response = EmailClient.getInstance().sendMail(ServiceRegistrarClient.getInstance().getServiceHostURL("Email"), vo.getCustomerAccount(), 
                        vo.getSiteName(), vo.getHostname(), vo.getException(), vo.getExceptionTime());
                int status = response.getStatusCode();
                if(status == 200){
                    return Response.status(200).header("Access-Control-Allow-Origin", "*").build();
                }
                else{
                    // send event
                    LOGGER.error("Unable to HTTP POST to Email Service. HTTP Response:"+status);
                    return Response.status(500).header("Access-Control-Allow-Origin", "*").build();
                }
            }
            return Response.status(400).header("Access-Control-Allow-Origin", "*").build();
        }
        catch(Exception e){
            LOGGER.error(e.getMessage(), e);
            return Response.status(500).header("Access-Control-Allow-Origin", "*").build();
        }
    }
    
    @PUT
    public Response putJson() {
        return Response.status(400).header("Access-Control-Allow-Origin", "*").build();
    }
 
    @DELETE
    public Response deleteJson() {
        return Response.status(400).header("Access-Control-Allow-Origin", "*").build();
    }
}
