/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.basestation.http.rest;


import com.datastax.oss.driver.api.core.uuid.Uuids;
import com.uptime.basestation.http.client.MonitoringClient;
import static com.uptime.basestation.http.utils.json.JsonUtil.parser;
import com.uptime.basestation.http.utils.json.sax.MonitoringSaxJsonParser;
import com.uptime.basestation.utils.vo.ApprovedBaseStationsVO;
import com.uptime.basestation.utils.vo.PasscodePostVO;
import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.client.utils.vo.HttpResponseVO;
import java.time.Clock;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.UUID;
import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.Context;
import javax.ws.rs.Produces;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * REST Web Service
 *
 * @author twilcox
 */
@Path("/authentication")
@Singleton
public class BaseStationAuthResource {
    private final static Logger LOGGER = LoggerFactory.getLogger(BaseStationAuthResource.class);
    
    @GET
    @Path("/hello")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getJson() {
        LOGGER.info("HELLO WORLD");
        return Response.ok("{\"outcome\":\"Hello World!\"}").header("Access-Control-Allow-Origin", "*").build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getJson(@Context HttpHeaders headers, @QueryParam("customer") String customer, @QueryParam("siteId") UUID siteId) {
        LOGGER.info("GET");
        List<ApprovedBaseStationsVO> approvedBaseStationsVOList;
        ApprovedBaseStationsVO approvedBaseStationsVO;
        HttpResponseVO httpResponseVO;
        long unixTimestampInMill;
        String mac, token;
        StringBuilder json;
        Instant now, tokenInstant;
        
        try {
            if(headers != null){
                // check query parameters
                if(customer != null && siteId != null){
                    // get the required HTTP headers to prove this is an authorized device
                    List<String> header = headers.getRequestHeader("HWID");
                    if(header != null && !header.isEmpty())
                        mac = header.get(0);
                    else
                        mac = null;
                    header = headers.getRequestHeader("HWIDTOKEN");
                    if(header != null && !header.isEmpty())
                        token = header.get(0);
                    else
                        token = null;
                    // check the required HTTP headers
                    if(mac != null && !mac.isEmpty() && token != null && !token.isEmpty()){
                        if ((httpResponseVO = MonitoringClient.getInstance().getApprovedBaseStationsByPK(ServiceRegistrarClient.getInstance().getServiceHostURL("Monitoring"), customer, siteId, mac)) != null && 
                            httpResponseVO.getEntity() != null && httpResponseVO.getStatusCode() == 200) {
                            if ((approvedBaseStationsVOList = MonitoringSaxJsonParser.getInstance().populateApprovedBaseStationsFromJson(httpResponseVO.getEntity())) != null && !approvedBaseStationsVOList.isEmpty() &&
                                (approvedBaseStationsVO = approvedBaseStationsVOList.get(0)) != null) {
                                if (approvedBaseStationsVO.getIssuedToken() != null && token.equals(approvedBaseStationsVO.getIssuedToken().toString())) {
                                    unixTimestampInMill = Uuids.unixTimestamp(approvedBaseStationsVO.getIssuedToken());
                                    tokenInstant = Instant.ofEpochMilli(unixTimestampInMill);
                                    now = Instant.now(Clock.systemUTC());
                                    
                                    if (tokenInstant.isAfter(now.minus(24, ChronoUnit.HOURS))) {
                                        approvedBaseStationsVO.setIssuedToken(Uuids.timeBased());
                                        if (MonitoringClient.getInstance().updateApprovedBaseStations(ServiceRegistrarClient.getInstance().getServiceHostURL("Monitoring"), approvedBaseStationsVO)) {
                                            json = new StringBuilder();
                                            json.append("{\"issuedToken\":\"").append(approvedBaseStationsVO.getIssuedToken().toString()).append("\",")
                                                    .append("\"outcome\":\"Token updated successfully.\"}");
                                            return Response.status(200).entity(json.toString()).header("Access-Control-Allow-Origin", "*").build();
                                        }
                                        else {
                                            LOGGER.error("Unable to refresh token for Customer:"+customer+" SiteId:"+siteId.toString()+" MAC:"+mac);
                                            return Response.status(503).header("Access-Control-Allow-Origin", "*").build();
                                        }
                                    }
                                    else {
                                        LOGGER.warn("Token is expired for Customer:"+customer+" SiteId:"+siteId.toString()+" MAC:"+mac);
                                        return Response.status(403).header("Access-Control-Allow-Origin", "*").build();
                                    }
                                }
                                else {
                                    LOGGER.warn("Tokens do not match for Customer:"+customer+" SiteId:"+siteId.toString()+" MAC:"+mac);
                                    return Response.status(403).header("Access-Control-Allow-Origin", "*").build();
                                }
                            }
                            else {
                                LOGGER.error("Base Station Auth parsing error occurred for Customer:"+customer+" SiteId:"+siteId.toString()+" MAC:"+mac);
                                return Response.status(500).header("Access-Control-Allow-Origin", "*").build();
                            }
                        }
                        else{
                            LOGGER.error("Unable to retreive base station info from service for Customer:"+customer+" SiteId:"+siteId.toString()+" MAC:"+mac);
                            return Response.status(503).header("Access-Control-Allow-Origin", "*").build();
                        }
                    }
                    else{
                        LOGGER.warn("Required Headers Missing - HWID:"+mac+" HWIDTOKEN:"+token);
                        return Response.status(400).header("Access-Control-Allow-Origin", "*").build();
                    }
                }
                else{
                    LOGGER.warn("Query Parameter Missing - Customer:"+customer+" SiteID:"+siteId);
                    return Response.status(400).header("Access-Control-Allow-Origin", "*").build();
                }
            }
            else{
                LOGGER.warn("No HTTP Headers");
                return Response.status(400).header("Access-Control-Allow-Origin", "*").build();
            }
        } 
        catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return Response.status(500).header("Access-Control-Allow-Origin", "*").build();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response postJson(@Context HttpHeaders headers, String body) {
        List<ApprovedBaseStationsVO> approvedBaseStationsVOList;
        ApprovedBaseStationsVO approvedBaseStationsVO;
        PasscodePostVO passcodePostVO;
        HttpResponseVO httpResponseVO;
        StringBuilder json;
        Instant now;
        String mac;
        
        try {
            LOGGER.info("BODY: "+body);
            LOGGER.info("HWID: "+headers.getRequestHeader("HWID").get(0));
            if (headers != null && body != null && (mac = headers.getRequestHeader("HWID").get(0)) != null && !mac.isEmpty()) {
                // get the list of approved basestations for this site and MAC address from the Monitoring service
                if ((passcodePostVO = parser(body)) != null && (httpResponseVO = MonitoringClient.getInstance().getApprovedBaseStationsByPK(ServiceRegistrarClient.getInstance().getServiceHostURL("Monitoring"), passcodePostVO.getCustomerAccount(), passcodePostVO.getSiteId(), mac)) != null && 
                        httpResponseVO.getEntity() != null && httpResponseVO.getStatusCode() == 200) {
                    // parse the JSON from the Monitoring service
                    if ((approvedBaseStationsVOList = MonitoringSaxJsonParser.getInstance().populateApprovedBaseStationsFromJson(httpResponseVO.getEntity())) != null && !approvedBaseStationsVOList.isEmpty() &&
                            (approvedBaseStationsVO = approvedBaseStationsVOList.get(0)) != null) {
                        if (approvedBaseStationsVO.getPasscode() != null) {
                            if (approvedBaseStationsVO.getPasscode().equals(passcodePostVO.getPasscode())) {
                                now = Instant.now(Clock.systemUTC());
                                if (approvedBaseStationsVO.getLastPasscode() == null || 
                                        (approvedBaseStationsVO.getLastPasscode().isBefore(now.minus(24, ChronoUnit.HOURS)))) {
                                    LOGGER.info("UPDATING TOKEN FOR "+mac);
                                    approvedBaseStationsVO.setLastPasscode(now);
                                    approvedBaseStationsVO.setIssuedToken(Uuids.timeBased());
                                    if (MonitoringClient.getInstance().updateApprovedBaseStations(ServiceRegistrarClient.getInstance().getServiceHostURL("Monitoring"), approvedBaseStationsVO)) {
                                        json = new StringBuilder();
                                        json
                                                .append("{\"issuedToken\":\"").append(approvedBaseStationsVO.getIssuedToken().toString()).append("\",")
                                                .append("\"outcome\":\"Token updated successfully.\"}");
                                        return Response.status(200).entity(json.toString()).header("Access-Control-Allow-Origin", "*").build();
                                    } else {
                                        LOGGER.error("Monitoring Service error occurred for "+mac);
                                        return Response.status(503).entity("{\"outcome\":\"Error: Monitoring Service error occurred.\"}").header("Access-Control-Allow-Origin", "*").build();
                                    }
                                } else {
                                    LOGGER.warn("Passcode has been used in the last 24 hours for "+mac);
                                    return Response.status(403).entity("{\"outcome\":\"Passcode has been used in the last 24 hours.\"}").header("Access-Control-Allow-Origin", "*").build();
                                }
                            } else {
                                LOGGER.warn("Passcode do not match for "+mac);
                                return Response.status(403).entity("{\"outcome\":\"Passcode doesn't equal the correct value.\"}").header("Access-Control-Allow-Origin", "*").build();
                            }
                        } else {
                            LOGGER.error("Monitoring Service error occurred for "+mac);
                            return Response.status(503).entity("{\"outcome\":\"Error: Monitoring Service error occurred.\"}").header("Access-Control-Allow-Origin", "*").build();
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return Response.status(500).entity("{\"outcome\":\"Error: Base Station Auth error occurred.\"}").header("Access-Control-Allow-Origin", "*").build();
        }
        return Response.status(400).entity("{\"outcome\":\"Invalid Values.\"}").header("Access-Control-Allow-Origin", "*").build();
    }
    
    @PUT
    public void putJson() {
        throw new UnsupportedOperationException();
    }
 
    @DELETE
    public void deleteJson() {
        throw new UnsupportedOperationException();
    }
}
