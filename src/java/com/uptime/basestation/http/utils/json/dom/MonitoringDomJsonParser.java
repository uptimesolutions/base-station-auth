/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.basestation.http.utils.json.dom;

import com.uptime.basestation.utils.vo.ApprovedBaseStationsVO;
import com.uptime.client.utils.DomJsonParser;
import com.uptime.client.utils.JsonConverterUtil;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author twilcox
 */
public class MonitoringDomJsonParser extends DomJsonParser {

    private static MonitoringDomJsonParser instance = null;

    /**
     * Private Singleton class
     */
    private MonitoringDomJsonParser() {
    }

    /**
     * Returns an instance of the class
     *
     * @return MonitoringDomJsonParser object
     */
    public static MonitoringDomJsonParser getInstance() {
        if (instance == null) {
            instance = new MonitoringDomJsonParser();
        }
        return instance;
    }

    /**
     * Convert a ApprovedBaseStationsVO Object to a String Object json
     *
     * @param approvedBaseStationsVO, ApprovedBaseStationsVO Object
     * @return String Object
     */
    public String getJsonFromMonitoring(ApprovedBaseStationsVO approvedBaseStationsVO) {
        try {
            if (approvedBaseStationsVO != null) {
                return JsonConverterUtil.toJSON(approvedBaseStationsVO);
            }
        } catch (Exception e) {
            Logger.getLogger(MonitoringDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }
}
