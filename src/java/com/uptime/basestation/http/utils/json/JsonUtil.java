/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.basestation.http.utils.json;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.uptime.basestation.utils.vo.BaseStationExceptionVO;
import com.uptime.basestation.utils.vo.PasscodePostVO;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author twilcox
 */
public class JsonUtil {
    private final static Logger LOGGER = LoggerFactory.getLogger(JsonUtil.class);

    /**
     * Parse the given json String Object and return a PasscodePostVO object
     *
     * @param content, String Object
     * @return PasscodePostVO Object
     */
    public static PasscodePostVO parser(String content) {
        PasscodePostVO passcodePostVO;
        JsonElement element;
        JsonObject jsonObject;

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonObject = element.getAsJsonObject()) != null) {
            if (jsonObject.has("customerAccount") && jsonObject.has("siteId") && jsonObject.has("passcode")) {
                try {
                    passcodePostVO = new PasscodePostVO();
                    passcodePostVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString().trim().toUpperCase());
                    passcodePostVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString()));
                    passcodePostVO.setPasscode(UUID.fromString(jsonObject.get("passcode").getAsString()));
                    return passcodePostVO;
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        }

        return null;
    }
    
    public static BaseStationExceptionVO parse(String content) {
        try{
            if (content != null && !content.isEmpty()){
                BaseStationExceptionVO vo = new BaseStationExceptionVO();
                JsonElement element = JsonParser.parseString(content);
                JsonObject obj = element.getAsJsonObject();
                vo.setCustomerAccount(obj.get("customer_acct").getAsString().trim().toUpperCase());
                vo.setSiteName(obj.get("site_id").getAsString().trim().toUpperCase());
                vo.setHostname(obj.get("hostname").getAsString().trim().toUpperCase());
                vo.setExceptionTime(obj.get("exception_time").getAsString());
                vo.setException(obj.get("exception").getAsString());
                return vo;
            }
            return null;
        }
        catch(Exception e){
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }
}
