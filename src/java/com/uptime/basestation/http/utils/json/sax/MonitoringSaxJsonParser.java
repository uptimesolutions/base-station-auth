/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.basestation.http.utils.json.sax;

import com.uptime.basestation.utils.vo.ApprovedBaseStationsVO;
import com.uptime.client.utils.SaxJsonParser;
import java.io.StringReader;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;

/**
 *
 * @author twilcox
 */
public class MonitoringSaxJsonParser extends SaxJsonParser {
    private static MonitoringSaxJsonParser instance = null;
    
    /**
     * Private Singleton class
     */
    private MonitoringSaxJsonParser() {
    }

    /**
     * Returns an instance of the class
     * @return MonitoringSaxJsonParser object
     */
    public static MonitoringSaxJsonParser getInstance() {
        if (instance == null) {
            instance = new MonitoringSaxJsonParser();
        }
        return instance; 
    }

    /**
     * Converts a json to a List Object of ApprovedBaseStationsVO Objects
     * @param content, String Object, json 
     * @return List Object of ApAlSetVO Objects
     */
    public List<ApprovedBaseStationsVO> populateApprovedBaseStationsFromJson(String content) {
        List<ApprovedBaseStationsVO> list = new ArrayList();
        String keyName;
        ApprovedBaseStationsVO approvedBaseStationsVO;
        boolean inArray;
         
        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            approvedBaseStationsVO = null;
            inArray = false;
            
            while (parser.hasNext()) {
                switch((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        approvedBaseStationsVO = null;
                        break;
                    case START_OBJECT:
                        approvedBaseStationsVO = inArray ? new ApprovedBaseStationsVO(): null;
                        break;
                    case END_OBJECT:
                        if(approvedBaseStationsVO != null && inArray)
                            list.add(approvedBaseStationsVO);
                        approvedBaseStationsVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                            switch(parser.getString()) {
                                case "customerAccount":
                                    keyName = "customerAccount";
                                    break;
                                case "siteId":
                                    keyName = "siteId";
                                    break;
                                case "macAddress":
                                    keyName = "macAddress";
                                    break;
                                case "issuedToken":
                                    keyName = "issuedToken";
                                    break;
                                case "lastPasscode":
                                    keyName = "lastPasscode";
                                    break;
                                case "passcode":
                                    keyName = "passcode";
                                    break;
                                default:
                                    keyName = null;
                                    break;      
                            }
                        break;
                    case VALUE_STRING:
                        if(approvedBaseStationsVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "customerAccount":
                                    approvedBaseStationsVO.setCustomerAccount(parser.getString());
                                    break;
                                case "siteId":
                                    try {
                                        approvedBaseStationsVO.setSiteId(UUID.fromString(parser.getString()));
                                    }catch (Exception ex) {}
                                    break;
                                case "macAddress":
                                    approvedBaseStationsVO.setMacAddress(parser.getString());
                                    break;
                                case "issuedToken":
                                    try {
                                        approvedBaseStationsVO.setIssuedToken(UUID.fromString(parser.getString()));
                                    }catch (Exception ex) {}
                                    break;
                                case "lastPasscode":
                                    try {
                                        approvedBaseStationsVO.setLastPasscode(LocalDateTime.parse(parser.getString(), DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_FORMAT)).atZone(ZoneOffset.UTC).toInstant());
                                    }catch (Exception ex) {}
                                    break;
                                case "passcode":
                                    try {
                                        approvedBaseStationsVO.setPasscode(UUID.fromString(parser.getString()));
                                    }catch (Exception ex) {}
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(MonitoringSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
}
