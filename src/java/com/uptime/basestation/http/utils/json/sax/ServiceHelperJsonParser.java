/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.basestation.http.utils.json.sax;

import com.uptime.client.utils.SaxJsonParser;
import java.io.StringReader;
import java.math.BigInteger;
import javax.json.Json;
import org.slf4j.LoggerFactory;

/**
 *
 * @author twilcox
 */
public class ServiceHelperJsonParser extends SaxJsonParser {
    private final static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(ServiceHelperJsonParser.class);
    private static ServiceHelperJsonParser instance = null;
    
    /**
     * Private Singleton class
     */
    private ServiceHelperJsonParser() {
    }

    /**
     * Returns an instance of the class
     * @return MonitoringSaxJsonParser object
     */
    public static ServiceHelperJsonParser getInstance() {
        if (instance == null) {
            instance = new ServiceHelperJsonParser();
        }
        return instance; 
    }

    private class ChannelVO{
        private String deviceId;
        private String channelType;
        private String sampleInterval;
        private String channelNum;
        private String fmax;
        private String resolution;
        private String channelId;
        private String lastSampled;
        private String pointId;

        public String getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(String deviceId) {
            // convert to BigInteger
            BigInteger val = new BigInteger(deviceId,16);
            this.deviceId = val.toString();
        }

        public String getChannelType() {
            return channelType;
        }

        public void setChannelType(String channelType) {
            this.channelType = channelType;
        }

        public String getSampleInterval() {
            return sampleInterval;
        }

        public void setSampleInterval(String sampleInterval) {
            this.sampleInterval = sampleInterval;
        }

        public String getChannelNum() {
            return channelNum;
        }

        public void setChannelNum(String channelNum) {
            this.channelNum = channelNum;
        }

        public String getFmax() {
            return fmax;
        }

        public void setFmax(String fmax) {
            this.fmax = fmax;
        }

        public String getResolution() {
            return resolution;
        }

        public void setResolution(String resolution) {
            this.resolution = resolution;
        }

        public String getChannelId() {
            return channelId;
        }

        public void setChannelId(String channelId) {
            this.channelId = channelId;
        }

        public String getLastSampled() {
            return lastSampled;
        }

        public void setLastSampled(String lastSampled) {
            this.lastSampled = lastSampled;
        }

        public String getPointId() {
            return pointId;
        }

        public void setPointId(String pointId) {
            this.pointId = pointId;
        }
    }

    public String formatServiceHelperResponse(String json){
        String keyName;
        ChannelVO vo = null;
        StringBuilder sb = null;
        
        try{
            parser = Json.createParser(new StringReader(json));
            keyName = null;
            boolean inPoints = false;
            boolean inPointsArray = false;
            String baseStationPort = null;
            boolean omit = false; // used for is_disabled
            sb = new StringBuilder();
            sb.append("{\"channels\":[");
            while (parser.hasNext()) {
                switch((event = parser.next())) {
                    case START_OBJECT:
                        if(inPoints && inPointsArray){
                            vo = new ChannelVO();
                        }
                        break;
                    case END_OBJECT:
                        if(inPoints && inPointsArray && !omit){ // only include if devices is not disabled
                            sb.append("[");
                            sb.append(baseStationPort+",");
                            sb.append(vo.getDeviceId()+",");
                            if(vo.getChannelType().compareToIgnoreCase("AC") == 0)
                                sb.append("0,");
                            else if(vo.getChannelType().compareToIgnoreCase("DC") == 0)
                                sb.append("1,");
                            else
                                throw new Exception("Unknown channel type: "+vo.getChannelType());
                            sb.append(vo.getSampleInterval()+",");
                            sb.append(vo.getChannelNum()+",");
                            sb.append(vo.getFmax()+",");
                            sb.append(vo.getResolution()+",");
                            sb.append("\""+vo.getPointId()+"\",\"");
                            sb.append(vo.getLastSampled()+"\",");
                            sb.append("null,null,false,0],");
                        }
                        keyName = null;
                        break;
                    case START_ARRAY:
                        if(inPoints){
                            inPointsArray = true;
                        }
                        break;
                    case END_ARRAY:
                        if(inPoints){
                            sb.deleteCharAt(sb.length()-1);
                            sb.append("],");
                            inPointsArray = false;
                            inPoints = false;
                        }
                        keyName = null;
                        break;
                    case KEY_NAME:
                        switch(parser.getString()) {
                            case "customer_acct":
                                keyName = "customer_acct";
                                break;
                            case "site_id":
                                keyName = "site_id";
                                break;
                            case "base_station_port":
                                keyName = "base_station_port";
                                break;
                            case "points":
                                keyName = "points";
                                inPoints = true;
                                break;
                            case "device_id":
                                keyName = "device_id";
                                break;
                            case "point_id":
                                keyName = "point_id";
                                break;
                            case "channel_type":
                                keyName = "channel_type";
                                break;
                            case "sensor_channel_num":
                                keyName = "sensor_channel_num";
                                break;
                            case "sample_interval":
                                keyName = "sample_interval";
                                break;
                            case "fmax":
                                keyName = "fmax";
                                break;
                            case "resolution":
                                keyName = "resolution";
                                break;
                            case "last_sampled":
                                keyName = "last_sampled";
                                break;
                            case "is_disabled":
                                keyName = "is_disabled";
                                break;
                        }
                        break;
                    case VALUE_STRING:
                        switch(keyName) {
                            case "base_station_port":
                                baseStationPort = parser.getString();
                                break;
                            case "device_id":
                                vo.setDeviceId(parser.getString());
                                break;
                            case "point_id":
                                vo.setPointId(parser.getString());
                                break;
                            case "channel_type":
                                vo.setChannelType(parser.getString());
                                break;
                            case "last_sampled":
                                vo.setLastSampled(parser.getString());
                                break;
                        }
                        break;
                    case VALUE_NUMBER:
                        switch(keyName) {
                            case "sensor_channel_num":
                                vo.setChannelNum(parser.getString());
                                break;
                            case "sample_interval":
                                vo.setSampleInterval(parser.getString());
                                break;
                            case "fmax":
                                vo.setFmax(parser.getString());
                                break;
                            case "resolution":
                                vo.setResolution(parser.getString());
                                break;
                        }
                        break;
                    case VALUE_FALSE:
                        switch(keyName) {
                            case "is_disabled":
                                omit = false;
                                break;
                        }
                        break;
                    case VALUE_TRUE:
                        switch(keyName) {
                            case "is_disabled":
                                omit = true;
                                break;
                        }
                        break;
                }
            }
            sb.append("\"tachometers\":[],\"echoBoxes\":[]}");
        }
        catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return sb.toString();
    }
    
    public static void main(String[] args){
        String json = "{\n" +
"  \"customer_acct\": \"90000\",\n" +
"  \"site_id\": \"187e4953-a9cc-47f1-a8be-0b98fd85cd9a\",\n" +
"  \"base_station_port\": \"702\",\n" +
"  \"points\": [\n" +
"    {\n" +
"      \"device_id\": \"BBC02FE7\",\n" +
"      \"point_id\": \"1a555029-7ef7-468b-9b6f-9097b48cad5c\",\n" +
"      \"channel_type\": \"DC\",\n" +
"      \"sensor_channel_num\": 0,\n" +
"      \"sample_interval\": 15,\n" +
"      \"fmax\": 0,\n" +
"      \"resolution\": 0,\n" +
"      \"is_disabled\": false,\n" +
"      \"last_sampled\": \"2023-07-12T10:30:52Z\"\n" +
"    },\n" +
"    {\n" +
"      \"device_id\": \"BBC02FE7\",\n" +
"      \"point_id\": \"40acf1a2-6381-43e2-8590-404fafcb750c\",\n" +
"      \"channel_type\": \"AC\",\n" +
"      \"sensor_channel_num\": 1,\n" +
"      \"sample_interval\": 15,\n" +
"      \"fmax\": 977,\n" +
"      \"resolution\": 1600,\n" +
"      \"is_disabled\": true,\n" +
"      \"last_sampled\": \"2023-07-12T10:30:41Z\"\n" +
"    },\n" +
"    {\n" +
"      \"device_id\": \"BBC02FE7\",\n" +
"      \"point_id\": \"82dc5ee0-848b-48a4-9bca-531d15a4c2de\",\n" +
"      \"channel_type\": \"DC\",\n" +
"      \"sensor_channel_num\": 1,\n" +
"      \"sample_interval\": 15,\n" +
"      \"fmax\": 0,\n" +
"      \"resolution\": 0,\n" +
"      \"is_disabled\": false,\n" +
"      \"last_sampled\": \"2023-07-12T10:30:52Z\"\n" +
"    }\n" +
"  ]\n" +
"}";
        ServiceHelperJsonParser parser = new ServiceHelperJsonParser();
        String response = parser.formatServiceHelperResponse(json);
        LOGGER.info(response);
    }
}
