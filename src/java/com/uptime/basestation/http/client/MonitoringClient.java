/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.basestation.http.client;

import com.uptime.basestation.http.utils.json.dom.MonitoringDomJsonParser;
import com.uptime.basestation.utils.vo.ApprovedBaseStationsVO;
import com.uptime.client.http.client.Client;
import com.uptime.client.utils.vo.HttpResponseVO;
import java.net.URLEncoder;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author twilcox
 */
public class MonitoringClient extends Client {
    private final static Logger LOGGER = LoggerFactory.getLogger(MonitoringClient.class);
    private static MonitoringClient instance = null;

    /**
     * Private Singleton class
     */
    private MonitoringClient() {
    }

    /**
     * Returns an instance of the class
     *
     * @return MonitoringClient object
     */
    public static MonitoringClient getInstance() {
        if (instance == null) {
            instance = new MonitoringClient();
        }
        return instance;
    }

    /**
     * Get ApprovedBaseStations Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param macAddress, String Object
     * @return HttpResponseVO Objects
     */
    public HttpResponseVO getApprovedBaseStationsByPK(String host, String customer, UUID siteId, String macAddress) {
        StringBuilder endPoint;
        LOGGER.info("SERVICE HOST:"+host);
        try {
            endPoint = new StringBuilder();
            endPoint.append("/basestation")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&mac=").append(URLEncoder.encode(macAddress, "UTF-8"));
            LOGGER.info("HTTP GET:"+endPoint.toString());
            return httpGet("Monitoring", host, endPoint.toString());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return null;
    }
    
    /**
     * Update an ApprovedBaseStations based on the given info
     * 
     * @param host String Object
     * @param approvedBaseStationsVO, ApprovedBaseStationsVO Object
     * @return 
     */
    public boolean updateApprovedBaseStations(String host, ApprovedBaseStationsVO approvedBaseStationsVO) {
        HttpResponseVO responseVO;
        LOGGER.info("SERVICE HOST:"+host);
        if ((responseVO = httpPut("Monitoring", host, "/basestation", MonitoringDomJsonParser.getInstance().getJsonFromMonitoring(approvedBaseStationsVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                LOGGER.info("Approved Base Station updated successfully.");
                return true;
            } else {
                LOGGER.warn("Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                LOGGER.warn("Error Message from Monitoring Service: {0}", MonitoringDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        return false;
    }
    
    public HttpResponseVO getServiceHelperConfig(String host, String customer, UUID siteId, int basestationPort){
        StringBuilder endPoint;
        LOGGER.info("SERVICE HOST:"+host);
        try {
            endPoint = new StringBuilder();
            endPoint.append("/servicehelper")
                    .append("?customerAcct=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&baseStationPort=").append(basestationPort);
            LOGGER.info("HTTP GET:"+endPoint.toString());
            return httpGet("Monitoring", host, endPoint.toString());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return null;
    }
}
