/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.basestation.http.client;

import com.uptime.client.http.client.Client;
import com.uptime.client.utils.vo.HttpResponseVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author twilcox
 */
public class EmailClient extends Client {
    private final static Logger LOGGER = LoggerFactory.getLogger(EmailClient.class);
    private static EmailClient instance = null;

    /**
     * Private Singleton class
     */
    private EmailClient() {
    }

    /**
     * Returns an instance of the class
     *
     * @return MonitoringClient object
     */
    public static EmailClient getInstance() {
        if (instance == null) {
            instance = new EmailClient();
        }
        return instance;
    }

    /**
     * 
     * @param serviceHost
     * @param customer
     * @param siteName
     * @param hostname
     * @param exception
     * @param exceptionTime
     * @return 
     */
    public HttpResponseVO sendMail(String serviceHost, String customer, String siteName, String hostname, String exception, String exceptionTime) {

        //LOGGER.info("SERVICE HOST:"+serviceHost);
        StringBuilder sb = new StringBuilder();
        sb.append("{\"from_email\":\"noreply@uptime-solutions.us\",");
        sb.append("\"to_emails\":[{\"to_email\":\"ksimmons@uptime-solutions.us\"}],");
        sb.append("\"subject\":\"Airbase Exception\",");
        sb.append("\"body\":\"Customer:").append(hostname).append("\n");
        sb.append("Site:").append(siteName).append("\n");
        sb.append("Airbase:").append(hostname).append("\n");
        sb.append("Exception Time:").append(exceptionTime).append("\n");
        sb.append("Exception:").append(exception).append("\"}");
        //LOGGER.info("JSON:"+sb.toString());
        try {
            return httpPost("Email", serviceHost, "/email", sb.toString());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return null;
    }
}
