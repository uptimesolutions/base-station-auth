/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.basestation.utils.vo;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author twilcox
 */
public class BaseStationExceptionVO implements Serializable {
    private String customerAccount;
    private String siteName;
    private String hostname;
    private String exceptionTime;
    private String exception;

    public BaseStationExceptionVO() {
    }

    public BaseStationExceptionVO(BaseStationExceptionVO passcodePostVO) {
        this.customerAccount = passcodePostVO.getCustomerAccount();
        this.siteName = passcodePostVO.getSiteName();
        this.hostname = passcodePostVO.getHostname();
        this.exception = passcodePostVO.getException();
        this.exceptionTime = passcodePostVO.getExceptionTime();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getExceptionTime() {
        return exceptionTime;
    }

    public void setExceptionTime(String exceptionTime) {
        this.exceptionTime = exceptionTime;
    }

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + Objects.hashCode(this.customerAccount);
        hash = 83 * hash + Objects.hashCode(this.siteName);
        hash = 83 * hash + Objects.hashCode(this.hostname);
        hash = 83 * hash + Objects.hashCode(this.exception);
        hash = 83 * hash + Objects.hashCode(this.exceptionTime);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BaseStationExceptionVO other = (BaseStationExceptionVO) obj;
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.siteName, other.siteName)) {
            return false;
        }
        if (!Objects.equals(this.hostname, other.hostname)) {
            return false;
        }
        if (!Objects.equals(this.exception, other.exception)) {
            return false;
        }
        if (!Objects.equals(this.exceptionTime, other.exceptionTime)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PasscodePostVO{" + "customerAccount=" + customerAccount + ", siteName=" + siteName + ", hostname=" + hostname + ", exception=" + exception + ", exceptionTime=" + exceptionTime +"}";
    }
}
