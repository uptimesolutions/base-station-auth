/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.basestation.utils.vo;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author twilcox
 */
public class PasscodePostVO implements Serializable {
    private String customerAccount;
    private UUID siteId;
    private UUID passcode;

    public PasscodePostVO() {
    }

    public PasscodePostVO(PasscodePostVO passcodePostVO) {
        this.customerAccount = passcodePostVO.getCustomerAccount();
        this.siteId = passcodePostVO.getSiteId();
        this.passcode = passcodePostVO.getPasscode();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getPasscode() {
        return passcode;
    }

    public void setPasscode(UUID passcode) {
        this.passcode = passcode;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + Objects.hashCode(this.customerAccount);
        hash = 83 * hash + Objects.hashCode(this.siteId);
        hash = 83 * hash + Objects.hashCode(this.passcode);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PasscodePostVO other = (PasscodePostVO) obj;
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.passcode, other.passcode)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PasscodePostVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", passcode=" + passcode + '}';
    }
}
