/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.basestation.utils.vo;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author twilcox
 */
public class ApprovedBaseStationsVO implements Serializable {
    private String customerAccount;
    private UUID siteId;
    private String macAddress;
    private UUID passcode;
    private UUID issuedToken;
    private Instant lastPasscode;

    public ApprovedBaseStationsVO() {
    }

    public ApprovedBaseStationsVO(ApprovedBaseStationsVO approvedBaseStationsVO) {
        this.customerAccount = approvedBaseStationsVO.getCustomerAccount();
        this.siteId = approvedBaseStationsVO.getSiteId();
        this.macAddress = approvedBaseStationsVO.getMacAddress();
        this.passcode = approvedBaseStationsVO.getPasscode();
        this.issuedToken = approvedBaseStationsVO.getIssuedToken();
        this.lastPasscode = approvedBaseStationsVO.getLastPasscode();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public UUID getPasscode() {
        return passcode;
    }

    public void setPasscode(UUID passcode) {
        this.passcode = passcode;
    }

    public UUID getIssuedToken() {
        return issuedToken;
    }

    public void setIssuedToken(UUID issuedToken) {
        this.issuedToken = issuedToken;
    }

    public Instant getLastPasscode() {
        return lastPasscode;
    }

    public void setLastPasscode(Instant lastPasscode) {
        this.lastPasscode = lastPasscode;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.customerAccount);
        hash = 17 * hash + Objects.hashCode(this.siteId);
        hash = 17 * hash + Objects.hashCode(this.macAddress);
        hash = 17 * hash + Objects.hashCode(this.passcode);
        hash = 17 * hash + Objects.hashCode(this.issuedToken);
        hash = 17 * hash + Objects.hashCode(this.lastPasscode);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ApprovedBaseStationsVO other = (ApprovedBaseStationsVO) obj;
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.macAddress, other.macAddress)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.passcode, other.passcode)) {
            return false;
        }
        if (!Objects.equals(this.issuedToken, other.issuedToken)) {
            return false;
        }
        if (!Objects.equals(this.lastPasscode, other.lastPasscode)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ApprovedBaseStationsVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", macAddress=" + macAddress + ", passcode=" + passcode + ", issuedToken=" + issuedToken + ", lastPasscode=" + lastPasscode + '}';
    }
}
